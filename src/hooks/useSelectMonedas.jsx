import { useState } from 'react';
import styled from '@emotion/styled'


const useSelectMonedas = (label, opciones) => {

    const [state,setState] = useState('')


    const SelectMonedas = () => (
        <div className='grid grid-cols-1 mt-3'>
            <label className='text-sm md:text-xl text-white'>{label} : </label>

            <select className='p-3 bg-indigo-200 text-center text-xs sm:text-sm md:text-base mt-5 text-indigo-900 font-semibold mb-4 border-2 border-violet-900 rounded shadow'
                value={state}
                onChange = {e => setState(e.target.value)}>
                <option value="">Seleccione</option>
                {opciones.map(opcion => (
                    <option 
                    key={opcion.id}
                    value={opcion.id}> 
                        {opcion.nombre}
                    </option>
                ))}
            </select>
        </div>
    )

    return [state,SelectMonedas]
}

export default useSelectMonedas