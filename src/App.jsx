import { useEffect, useState } from "react";
import ImagenCripto from "./img/criptomonedas.png";
import Formulario from "./components/Formulario";
import Cotizacion from "./components/Cotizacion";
import Spinner from "./components/Spinner";
import SquareBg from "./components/SquareBg";

const App = () => {
  const [monedas, setMonedas] = useState({});
  const [cotizacion, setCotizacion] = useState({});
  const [cargando, setCargando] = useState(false);

  useEffect(() => {
    if (Object.keys(monedas).length > 0) {
      const consultarAPI = async () => {
        setCargando(true); // Spinner cargando
        const { moneda, criptomoneda } = monedas;

        const url = `https://min-api.cryptocompare.com/data/pricemultifull?fsyms=${criptomoneda}&tsyms=${moneda}`;
        const respuesta = await fetch(url);
        const resultado = await respuesta.json();

        setCotizacion(resultado.DISPLAY[criptomoneda][moneda]);
        setCargando(false);
      };

      consultarAPI();
    }
  }, [monedas]);

  return (
    <div className="h-dvh w-full relative overflow-hidden">
      <SquareBg />
      <div className="absolute inset-0 flex items-center justify-center transform scale-100">
        <div className="flex flex-col md:flex-col xl:flex-row justify-center shadow-2xl rounded-lg overflow-hidden max-w-full w-[90%] md:w-[60%] xl:w-[75%]">
          {/* Contenedor de la imagen */}
          <div className="flex items-center justify-center bg-violet-800 flex-[50%] py-4  md:py-0 rounded-t xl:rounded-s">
            <img
              src={ImagenCripto}
              alt="imagenes criptomonedas"
              className="drop-shadow-xl w-[3rem] sm:w-[5rem] md:w-[8rem] md:py-5  xl:w-[20rem] mx-20"
            />
          </div>

          {/* Contenedor del contenido */}
          <div className="p-9 flex-[50%] bg-violet-900 rounded-b lg:rounded-b-none xl:rounded-e overflow-hidden">
            <div className="py-2 sm:py-4 border-b-4 border-violet-700">
              <p className="text-lg md:text-3xl text-center text-white font-semibold">
                Cotizador criptomonedas<br/>
                <span className="text-sm underline text-violet-50"> Por Daniel Roa</span>
              </p>
            </div>
            <Formulario setMonedas={setMonedas} />
            {cargando && <Spinner />}
            {cotizacion && cotizacion.PRICE && (
              <Cotizacion cotizacion={cotizacion} />
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default App;
