import React from 'react'

const SquareBg = () => {
    return (
        <ul className='circles bg-violet-600'>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
        </ul>
    )
}

export default SquareBg