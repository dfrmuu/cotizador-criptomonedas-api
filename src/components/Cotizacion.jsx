import styled from '@emotion/styled'
import Swal from 'sweetalert2'

const Cotizacion = ({cotizacion}) => {

  const {PRICE, HIGHDAY, LOWDAY, IMAGEURL, LASTUPDATE} = cotizacion

  return (

    <div className='flex justify-center bg-violet-500 p-3 mt-4 rounded text-xs sm:text-sm md:text-base'>
        <img src={`https://www.cryptocompare.com/${IMAGEURL}`} alt="Imagen criptomoneda" 
            className='w-[3rem] sm:w-[5rem] md:w-[8rem] lg:w-[7rem] self-center rounded-full mx-5 drop-shadow-lg'/>
        <div className='text-indigo-50 self-center'>
            <p className='font-semibold'> Precio en moneda: <span> {PRICE} </span> </p>
            <p className='font-semibold'> Precio más alto: <span> {HIGHDAY} </span> </p>
            <p className='font-semibold'> Precio más bajo: <span> {LOWDAY} </span> </p>
            <p className='font-semibold'> Ultima actualización: <span> {LASTUPDATE} </span> </p>
        </div>
    </div>
  )
}

export default Cotizacion 