import "../styles/Spinner.css";

const Spinner = () => {
  return (
    <>
      <div className="spinner mt-6 mb-6">
          <div className="bounce1"></div>
          <div className="bounce2"></div>
          <div className="bounce3"></div>
      </div>
    </>
  )
}

export default Spinner