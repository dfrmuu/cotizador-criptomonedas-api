import styled from '@emotion/styled'


const Error = ({children}) => {
  return (
    <p className='bg-violet-400 p-3 text-sm  md:text-base rounded shadow mb-2 mt-2 text-violet-900 font-semibold text-center'>
        {children}
    </p>
  )
}

export default Error