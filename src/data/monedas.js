
const monedas = [
        {id: 'USD' , nombre: 'Dolar estadounidense'},
        {id: 'MXN' , nombre: 'Peso mexicano'},
        {id: 'COP' , nombre: 'Peso colombiano'},
        {id: 'EUR' , nombre: 'Euros'}
]

export {
    monedas,
}